# Node.js

**Materials taken from:** 
> [How programming works](https://github.com/HowProgrammingWorks)

**[Node.js starter kit](./nodejsStarteKit/)**


## **Treads**
1. **[Atomix](./treads/atomix/)** - using Atomix
2. **[Buffers](./treads/buffers/)** - using Buffers
3. **[Treads](./treads/treads/)** - working with treads
4. **[Semaphore](./treads/semaphore/)** - semaphore example
5. **[Mutex](./treads/mutex/)** - mutex example

## **Sandboxes**
1. **[Sandbox](./sandbox/)** - sandbox example, **'vm' lib**

## Timers
1. **[Timers](./timers/)** - using timers

## Event Emitter
1. **[Event Emitter](./eventEmitter/)** - event emitter example. **'events' lib**

## **Process comunication**
1. **[Child process](./process/child-process/)** - child-process communication examples. **'child-process' lib**
2. **[Cluster](./process/cluster/)** - cluster inter process communication examples. **'child-process' lib**
3. **[TCP](./process/tcp/)** - tcp inter process communication examples. **'net' lib**

## **Streams**
1. **[streams](../streams/)** - **'stream' lib**, [revealing constructor](../streams/revealingConstructor/)

## **File system**
1. **[Streams](./files/streams/)** - sockets streams examples
2. **[Files](./files/files/)** - files handle examples. 'fs' lib
3. **[Files watching](./files/watch/)** - watching files example
4. **[Live reload](./files/liveReload/)** - live example
5. **[Logging](./files/logging/)** - logging examples

## **Node.js Servers**
1. **[Sockets](./servers/sockets/)** - TCP, UDP sockets examples. **'net' lib**
2. **[Static server](./servers/serveStatic/)**
3. **[HTTP servers](./servers/httpServers/)** - http server examples. **'http', 'https' lib**
4. **[Graceful shutdown](./servers/gracefulShutdown/)** - server graceful shutdown examples
5. **[Session and cookies](./servers/session/)** - http server with session and cookies example
6. **[Actors model](./servers/actorsModels/)** - actor models for parallel calculations example
7. **[workers server](./servers/wotkers/)** - server with workers example

## **Packages from npm**
1. **web socket [chat](./packages/webSocket/chat/) & [live table](./packages/webSocket/liveTable/)** - web socket,  examples, **module('ws')** 
2. **[postrgeSQL](./packages/postgreSQL/01.example/)** - working with PostrgeSQL, example of [self query builder](./packages/postgreSQL/01.example/db.js), **module('pg')**
3. **[Rx JS](./packages/rxjs/)** - Rx JS example, **module('rxjs')** 
4. **express** - for http server based on middlewares
5. **mongoose** - for working with mongoDB
6. **telegram-api** - for working wit telegram bot API
7. **do** - for chaining operations
8. **metasync** - function composition for async I/O